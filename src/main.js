// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import system from 'vue-example-design-system';
// import 'vue-example-design-system/dist/system/system.css';
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import Icon from 'vue-awesome/components/Icon';
import 'vue-awesome/icons';
import 'open-sans-all/css/open-sans.min.css';
import App from './App';
import router from './router';
import Announcement from './components/Announcement';
import Bottombar from './components/Bottombar';

Vue.use(BootstrapVue);
// Vue.use(system);
Vue.config.productionTip = false;
Vue.component('icon', Icon);
Vue.component('announcement', Announcement);
Vue.component('bottombar', Bottombar);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
