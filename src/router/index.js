import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import PastFlights from '@/components/PastFlights';
import ActiveFlights from '@/components/ActiveFlights';
import NewFlights from '@/components/NewFlights';
// import 'vue-awesome/icons';
// import 'vue-awesome/icons/flag';
// import Topbar from '@/components/Topbar';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/past',
      name: 'PastFlights',
      component: PastFlights,
    },
    {
      path: '/active',
      name: 'ActiveFlights',
      component: ActiveFlights,
    },
    {
      path: '/new',
      name: 'NewFlights',
      component: NewFlights,
    },
  ],
});
